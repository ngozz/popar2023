using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleScaleCrystal : MonoBehaviour
{
    public enum EffectType
    {
        Scale,
        Fade
    }

    public EffectType effectType;

    private ParticleSystem ps;

    Vector2 originStartScale;
    Color originStartColor;

    List<ParticleSystem.Particle> enter = new List<ParticleSystem.Particle>();
    List<ParticleSystem.Particle> exit = new List<ParticleSystem.Particle>();

    private void OnEnable()
    {
        ps = GetComponent<ParticleSystem>();
        var main = ps.main;
        switch (effectType)
        {
            case EffectType.Fade:
                originStartColor = main.startColor.color;
                break;
            case EffectType.Scale:
                originStartScale = new Vector2(main.startSize.constantMin, main.startSize.constantMax);
                break;
        }
    }

    void OnParticleTrigger()
    {
        int numEnter = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Enter, enter);
        int numExit = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Exit, exit);
        switch (effectType)
        {
            case EffectType.Fade:
                for (int i = 0; i < numEnter; i++)
                {
                    ParticleSystem.Particle p = enter[i];
                    p.startColor = new Color32(255, 255, 255, 0);
                    enter[i] = p;
                }

                // iterate through the particles which exited the trigger and make them green
                for (int i = 0; i < numExit; i++)
                {
                    ParticleSystem.Particle p = exit[i];
                    p.startColor = originStartColor;
                    exit[i] = p;
                }
                break;
            case EffectType.Scale:
                for (int i = 0; i < numEnter; i++)
                {
                    ParticleSystem.Particle p = enter[i];
                    p.startSize = 0f;
                    enter[i] = p;
                }

                for (int i = 0; i < numExit; i++)
                {
                    ParticleSystem.Particle p = exit[i];
                    p.startSize = Random.Range(originStartScale.x, originStartScale.y);
                    exit[i] = p;
                }
                break;
        }

        // re-assign the modified particles back into the particle system
        ps.SetTriggerParticles(ParticleSystemTriggerEventType.Enter, enter);
        ps.SetTriggerParticles(ParticleSystemTriggerEventType.Exit, exit);
    }
}
