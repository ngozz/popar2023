using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleScaleControllers : MonoBehaviour
{
    [SerializeField] float TextWaitTime;

    [SerializeField] float TextRunTime;

    [SerializeField] float TextFadeOutTime;

    [SerializeField] Vector3 StartScale;

    [SerializeField] Vector3 EndScale;

    [SerializeField] Transform Text;
    
    [Header("Play Fade Effect Time")]

    [SerializeField] int Year;

    [SerializeField] int Month;

    [SerializeField] int Day;

    [SerializeField] int Hour;

    [SerializeField] int Minute;

    [SerializeField] int Second;

    [Header("Setting Particle Fade")]

    [SerializeField] float ParticleFadeOutTime;

    [SerializeField] Vector3 ParticleEndScale;

    //[SerializeField] Color32 StartTextColor;

    //[SerializeField] Color32 EndTextColor;


    [SerializeField] ParticleSystem[] ps;

    private void Start()
    {
        StartCoroutine(TextsEffect());
        StartCoroutine(ParticlesEffect());
    }

    IEnumerator TextsEffect()
    {
        yield return new WaitForSeconds(TextWaitTime);
        float time = 0;
        while (time < TextRunTime)
        {
            time += Time.deltaTime;
            Text.localScale = Vector3.Lerp(StartScale, EndScale, time / TextRunTime);
            yield return null;
        }
        Text.localScale = StartScale;
        DateTime PlayFadeEffectTime = new DateTime(Year, Month, Day, Hour, Minute, Second);
        float ParticleWaitTime = (float)(PlayFadeEffectTime - DateTime.Now).TotalSeconds;
        yield return new WaitForSeconds(ParticleWaitTime);
        time = 0;
        while (time < TextFadeOutTime)
        {
            time += Time.unscaledDeltaTime;
            Text.localScale = Vector3.Lerp(EndScale, StartScale, time / TextFadeOutTime);
            yield return null;
        }
    }

    IEnumerator ParticlesEffect()
    {
        DateTime PlayFadeEffectTime = new DateTime(Year, Month, Day, Hour, Minute, Second);
        float ParticleWaitTime = (float)(PlayFadeEffectTime - DateTime.Now).TotalSeconds;
        yield return new WaitForSeconds(ParticleWaitTime);
        float time = 0;
        List<float> particleMinSize = new List<float>();
        List<float> particleMaxSize = new List<float>();
        for (int i = 0; i < ps.Length; i++)
        {
            var main = ps[i].main;
            particleMinSize.Add(main.startSize.constantMin);
            particleMaxSize.Add(main.startSize.constantMax);
        }
        while (time < ParticleFadeOutTime)
        {
            float preTime = time;
            time += Time.unscaledDeltaTime;
            for (int i = 0; i < ps.Length; i++)
            {
                ParticleSystem.Particle[] particles = new ParticleSystem.Particle[ps[i].particleCount];
                ps[i].GetParticles(particles);
                if (particles.Length < 1)
                    continue;
                ParticleSystem.MinMaxCurve minMaxCurve = new ParticleSystem.MinMaxCurve();
                var main = ps[i].main;
                minMaxCurve.constantMin = particleMinSize[i] - particleMinSize[i] * (time / Time.deltaTime);
                minMaxCurve.constantMax = particleMaxSize[i] - particleMaxSize[i] * (time / Time.deltaTime);
                main.startSize = minMaxCurve;
                for (int j = 0; j < particles.Length; j++)
                {
                    float startSize = particles[j].startSize;
                    particles[j].startSize = startSize - startSize * (Time.unscaledDeltaTime / (ParticleFadeOutTime - preTime));
                }
                ps[i].SetParticles(particles);
            }


            
            yield return null;
        }
    }
}
