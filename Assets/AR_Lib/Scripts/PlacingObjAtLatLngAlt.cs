using System;
using Google.XR.ARCoreExtensions;
using Google.XR.ARCoreExtensions.Samples.Geospatial;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
namespace JohnBui
{
    public class PlacingObjAtLatLngAlt : MonoBehaviour
    {
        
        [SerializeField] AREarthManager EarthManager;
      
        [SerializeField] VpsInitializer Initializer;
       
        [SerializeField] Text OutputText;
        
        [SerializeField] double HeadingThreshold = 25;
       
        [SerializeField] double HorizontalThreshold = 20;

      
        [SerializeField] double Latitude;
       
        [SerializeField] double Longitude;
       
        [SerializeField] double Altitude;
     
        [SerializeField] double Heading;
       
        [SerializeField] GameObject ContentPrefab;
      
        GameObject displayObject;
        
        [SerializeField] ARAnchorManager AnchorManager;
        bool initialized = false;

        // Update is called once per frame
        void Update()
        {
         
            if (!Initializer.IsReady || EarthManager.EarthTrackingState != TrackingState.Tracking)
            {
                return;
            }
          
            string status = "";
         
            GeospatialPose pose = EarthManager.CameraGeospatialPose;
          
            if (pose.HeadingAccuracy > HeadingThreshold ||
                  pose.HorizontalAccuracy > HorizontalThreshold)
            {
                status = "Low Accuracy: Look around";
            }
            else
            {
                status = "High precision：High Tracking Accuracy";
                if (!initialized)
                {
                    initialized = true;
                  
                    SpawnObject(pose, ContentPrefab);
                }
            }
          
            ShowTrackingInfo(status, pose);
        }

       
        void SpawnObject(GeospatialPose pose,GameObject prefab)
        {
           
            Altitude = pose.Altitude - 1.5f;

          
            //Create a rotation quaternion that has the +Z axis pointing in the same direction as the heading value (heading=0 means north direction)
            //https://developers.google.com/ar/develop/unity-arf/geospatial/developer-guide-android#place_a_geospatial_anchor
            Quaternion quaternion = Quaternion.AngleAxis(180f - (float)Heading, Vector3.up);

           
            ARGeospatialAnchor anchor = AnchorManager.AddAnchor(Latitude, Longitude, Altitude, quaternion);

            
            if (anchor != null)
            {
                displayObject = Instantiate(ContentPrefab, anchor.transform);
            }
        }
        void ShowTrackingInfo(string status, GeospatialPose pose)
        {
            if (OutputText == null) return;
            OutputText.text = string.Format(
               "\n" +
               "Latitude/Longitude: {0}°, {1}°\n" +
               "Horizontal Accuracy: {2}m\n" +
               "Altitude: {3}m\n" +
               "Vertical Accuracy: {4}m\n" +
               "Heading: {5}°\n" +
               "Heading Accuracy: {6}°\n" +
               "{7} \n"
               ,
               pose.Latitude.ToString("F6"),  //{0}
               pose.Longitude.ToString("F6"), //{1}
               pose.HorizontalAccuracy.ToString("F6"), //{2}
               pose.Altitude.ToString("F2"),  //{3}
               pose.VerticalAccuracy.ToString("F2"),  //{4}
               pose.Heading.ToString("F1"),   //{5}
               pose.HeadingAccuracy.ToString("F1"),   //{6}
               status //{7}
           );
        }
    }
}