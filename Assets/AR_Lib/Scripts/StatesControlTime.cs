using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Playables;

namespace JohnBui
{
    public class StatesControlTime : MonoBehaviour
    {
        //[SerializeField] Transform marker01;
        //[SerializeField] Transform marker02;
        //[SerializeField] Transform marker03;


        //[SerializeField] GameObject objPreshow, objShow5mins, objShow10mins;
        // [SerializeField] GameObject objCrystals;
        // [SerializeField] GameObject ARCamera;

        [SerializeField] ARTrackedImageManager m_TrackedImageManager;


        // [SerializeField] GameObject objImgTarget3;

        //[SerializeField] MessageController messageController;

        public GameObject gmShow01, gmShow02, gmShow03, gmShowFireWork;

        public PlayableDirector director;
        // public GameObject timeLineStates;
        public string gmName = "10min";
        public string trackImg1 = "track_m1";
        public string trackImg2 = "img_track";
        public string trackImg3 = "Show3";

        public GameObject debugStatus;
        public bool mocktCheckFW = false;
        private int tapMockt = 0;

        public bool isTrackedMark1 = false, isTrackedMark2 = false, isTrackedMark3 = false;

        public bool stilTracking = false;

        GameObject spawnedObject;

        public bool isDetec = false;
        bool isConnect = true;
        bool isActiveShow3 = false;
        float timeActiveShow3 = 0.033f;
        float trackTimeShow3 = 0.033f;
        DateTime now = DateTime.Now;

        private void Start()
        {
            mocktCheckFW = false;
            tapMockt = 0;
            resetStatus();
            gmShow03.SetActive(false);

            //StartCoroutine(checkInternetConnection());

            //DateTime currentDateTime = WorldTimeAPI.Instance.GetCurrentDateTime();
            //Debug.Log(currentDateTime.Hour + " : " + currentDateTime.Minute + " : " + currentDateTime.Second);
        }

        private void resetStatus(){
            isTrackedMark1 = true;
            isTrackedMark2 = true;
            gmShow01.SetActive(false);
            gmShowFireWork.SetActive(false);
        }

        public void tapCheckMockt()
        {
            tapMockt++;
            if(tapMockt > 4)
            {
                mocktCheckFW = true;
                debugStatus.SetActive(true);
            }
            else
            {
                mocktCheckFW = false;
                debugStatus.SetActive(false);
            }
        }

    

        private void Update()
        {
            DateTime now = DateTime.Now;
            int currentHour = now.Hour;

            foreach (var trackedImage in m_TrackedImageManager.trackables)
            {
                if (trackedImage.trackingState == UnityEngine.XR.ARSubsystems.TrackingState.Tracking)
                {                 
                    if (trackedImage.referenceImage.name == trackImg1)
                    {
                        if ((currentHour == 0 && now.Minute == 0 && !isTrackedMark1) || mocktCheckFW)
                        {
                            gmShowFireWork.SetActive(true);
                            gmShowFireWork.transform.position = trackedImage.transform.position;
                            gmShowFireWork.transform.rotation = trackedImage.transform.rotation;
                        }
                        else
                        {
                            gmShow01.SetActive(true);
                            gmShow01.transform.position = trackedImage.transform.position;
                            gmShow01.transform.rotation = trackedImage.transform.rotation;
                        }

                     

                        //Marker2: 420                       
                        
                        if (isTrackedMark1)
                        {
                            trackStates(0);
                            isTrackedMark3 = true;
                            isTrackedMark1 = false;
                        }


                        if (!isTrackedMark1 )
                        {
                            Debug.Log("show3 turn off");
                            isTrackedMark3 = true;
                            gmShow03.SetActive(false);
                        }

                    }

                    if (trackedImage.referenceImage.name == trackImg2)
                    {
                        gmShow01.SetActive(true);
                        gmShow01.transform.position = trackedImage.transform.position;
                        gmShow01.transform.rotation = trackedImage.transform.rotation;
                        //Marker2: 360
                        

                         if (isTrackedMark2)
                        {
                            trackStates(360);
                            isTrackedMark3 = true;
                            isTrackedMark2 = false;
                        }


                        if (!isTrackedMark2 )
                        {
                            Debug.Log("show3 turn off");
                            gmShow03.SetActive(false);
                        }
                    }
                    // float detectTime = 0;
                    if (trackedImage.referenceImage.name == trackImg3)
                    {
                        //Saving the component for future use 
                        //ParticleSystem[] particles = gmShow03.GetComponentsInChildren<ParticleSystem>();
                        
                        if (!isTrackedMark1 || !isTrackedMark2)
                        {
                            Debug.Log("show3 turn off");
                            gmShow03.SetActive(false);
                        }
                        else
                        {
                            //if (particles != null)
                            //{
                            //    foreach (ParticleSystem particle in particles)
                            //    {
                            //        particle.Play(); //Here we use the Play function to start the particle system
                            //    }
                                
                            //}
                            // while(isTrackedMark3)
                            // {
                            //     detectTime += Time.unscaledDeltaTime;
                            // }
                            
                            
                            // Debug.Log("Detect Time: "+now.Minute+":"+now.Second+":"+now.Millisecond);
                            // Debug.Log("AR camera position: " + ARCamera.transform.position + "AR camera rotation: " + ARCamera.transform.rotation);
                            timeActiveShow3 -= Time.unscaledDeltaTime;
                            gmShow03.SetActive(true);
                            if (timeActiveShow3 <=0)
                                {
                                    timeActiveShow3 = trackTimeShow3;
                                    gmShow03.transform.position = Vector3.Lerp(gmShow03.transform.position, trackedImage.transform.position, trackTimeShow3);
                                    gmShow03.transform.rotation = Quaternion.Lerp(gmShow03.transform.rotation, trackedImage.transform.rotation, trackTimeShow3);
                                    // isActiveShow3 = true;
                                }

                        }


                        if (isTrackedMark3)
                        {
                          

                            //isTrackedMark3 = false;
                        }
                    } 
                    // else 
                    // {
                    //     detectTime = 0;
                    // }

                    if (!stilTracking)
                    {
                        // timeLineStates = GameObject.Find(gmName);
                        // director = timeLineStates.GetComponent<PlayableDirector>();
                        stilTracking = true;
                    }
                }

            }

            //DateTime currentDateTime = WorldTimeAPI.Instance.GetCurrentDateTime();
            //Debug.Log(currentDateTime.Hour + " : " + currentDateTime.Minute + " : " + currentDateTime.Second);

            if (stilTracking)
            {
               
                if (isConnect)
                {
                    CheckMarkerByTime(now);
                    //CheckMarker01(currentDateTime);
                    //CheckMarker02(currentDateTime);
                }
                else
                {
                    CheckMarkerByTime(now);
                    //CheckMarker01(now);
                    //CheckMarker02(now);
                }
            }

        
            //CheckMarker03();
            //if (spawnedObject != null || !sttMark1 || !sttMark2 || !sttMark3)
            //{
            //    isDetec = true;
            //}
            //else if (spawnedObject == null && sttMark1 && sttMark2 && sttMark3)
            //{
            //    isDetec = false;
            //}

        }

        public void setIsTrack3(bool status)
        {
            isTrackedMark3 = status;
        }


        void trackStates(double time)
        {
           
            //director.Stop();
            director.time = time;

        }
        IEnumerator AvtiveContentKV(float waitTime, Transform transform)
        {
            float time = 0;
            gmShow03.SetActive(true);
            gmShow03.transform.position = transform.position;
            gmShow03.transform.rotation = transform.rotation;
            while (time < waitTime)
            {
                float preTime = time;
                time += Time.unscaledDeltaTime;
                
                if(time == waitTime)
                {
                    gmShow03.SetActive(false);
                    isActiveShow3 = false;
                }
                yield return null;
            }
            
               
        }
        IEnumerator checkInternetConnection()
        {
            WWW www = new WWW("http://google.com");
            yield return www;
            if (www.error != null)
            {
                isConnect = false;
            }
            else
            {
                isConnect = true;
            }
        }

        void CheckMarkerByTime(DateTime time)
        {
            int currentMin = time.Minute % 10;
            Debug.Log($" Min: {currentMin}, second: {time.Second}");
        

            int currentScond = currentMin * 60 + time.Second;
            Debug.Log("track show :"+ currentScond);
            // if (currentMin <= 9 && (time.Second >= 0 && time.Second <= 2))
            // {
            //     currentScond = currentMin * 60 + time.Second; 
            //     trackStates(currentScond);
            //     Debug.Log("track show");
            // }

            if( currentMin == 0 && isTrackedMark1)
            {
                trackStates(currentScond);
                director.Play();
                Debug.Log("0");
            }


            if (currentMin == 6 && time.Second == 0)
            {
                // resetStatus();                
                director.Stop();   
                //  gmShow01.SetActive(false);         
                Debug.Log("track show 1");

                isTrackedMark1 = true;
                isTrackedMark2 = true;
            }

            if (currentMin == 6 && !isTrackedMark2)
            {
                trackStates(currentMin * 60 + time.Second);
                director.Play();
                Debug.Log("360");
            }

           
            if (currentMin == 1 && time.Second == 0)
            {
              
                currentScond = 60;
                trackStates(currentScond);
                Debug.Log("track show 1");
            }



            if (currentMin == 7 && time.Second == 0)
            {

                currentScond = 420;
                trackStates(currentScond);
                Debug.Log("track show 2");
            }

             if (currentMin == 9 && (time.Second >= 58 &&  time.Second <= 59) ){
                        resetStatus();
             }

           
          
          

        }

        bool sttMark1 = true;
        //void CheckMarker01(DateTime time)
        //{
        //    if (time.Hour == 1 || time.Hour == 2 || time.Hour == 3 || time.Hour == 4 || time.Hour == 5 || time.Hour == 6 || time.Hour == 7 || time.Hour == 8 || time.Hour == 9
        //        || time.Hour == 10 || time.Hour == 11 || time.Hour == 12 || time.Hour == 13 || time.Hour == 14 || time.Hour == 15 || time.Hour == 16 || time.Hour == 17 || time.Hour == 18
        //         || time.Hour == 19 || time.Hour == 20 || time.Hour == 21 || time.Hour == 22)
        //    {
        //        if (time.Minute == 0 || time.Minute == 10 || time.Minute == 20 || time.Minute == 30 || time.Minute == 40 || time.Minute == 50)
        //        {
        //            if (time.Second < 30)
        //            {
        //                objImgTarget3.SetActive(true);
        //            }
        //            else
        //            {
        //                if (!sttMark1)
        //                {
        //                    objImgTarget3.SetActive(false);
        //                }
        //            }
        //            //marker 1 tracked found
        //            if (isTrackedMark1)
        //            {
        //                if (sttMark1)
        //                {
        //                    if (time.Second < 57)
        //                    {
        //                        if (spawnedObject == null)
        //                        {
        //                            spawnedObject = Instantiate(objPreshow, marker01.position, Quaternion.identity);
        //                        }
        //                        else
        //                        {
        //                            Destroy(spawnedObject);
        //                            spawnedObject = Instantiate(objPreshow, marker01.position, Quaternion.identity);
        //                        }
        //                    }

        //                    //show mess calib success
        //                    messageController.TogglePopupCalibSuccess(1, 0.5f);

        //                    //Set bool
        //                    isTrackedMark1 = false;
        //                    sttMark1 = false;
        //                    sttMark2 = true;
        //                    sttMark3 = true;
        //                    messageController.isCalibMess = false;
        //                }
        //            }
        //        }
        //        else if (time.Minute == 1 || time.Minute == 11 || time.Minute == 21 || time.Minute == 31 || time.Minute == 41 || time.Minute == 51)
        //        {
        //            if (!isTrackedMark1)
        //            {
        //                objImgTarget3.SetActive(false);
        //                if (spawnedObject == null)
        //                {
        //                    spawnedObject = Instantiate(objShow5mins, marker01.position, Quaternion.identity);
        //                }
        //                else
        //                {
        //                    Destroy(spawnedObject);
        //                    spawnedObject = Instantiate(objShow5mins, marker01.position, Quaternion.identity);
        //                }
        //                messageController.TogglePopupCalibSuccess(0, 0.5f);
        //                sttMark1 = true;
        //            }
        //        }
        //    }
        //    else if (time.Hour == 23)
        //    {
        //        if (time.Minute == 0 || time.Minute == 10 || time.Minute == 20 || time.Minute == 30 || time.Minute == 40 || time.Minute == 50)
        //        {
        //            if (time.Second < 30)
        //            {
        //                objImgTarget3.SetActive(true);
        //            }
        //            else
        //            {
        //                if (!sttMark1)
        //                {
        //                    objImgTarget3.SetActive(false);
        //                }
        //            }
        //            if (isTrackedMark1)
        //            {
        //                if (sttMark1)
        //                {
        //                    if (time.Second < 57)
        //                    {
        //                        if (spawnedObject == null)
        //                        {
        //                            spawnedObject = Instantiate(objPreshow, marker01.position, Quaternion.identity);
        //                        }
        //                        else
        //                        {
        //                            Destroy(spawnedObject);
        //                            spawnedObject = Instantiate(objPreshow, marker01.position, Quaternion.identity);
        //                        }
        //                    }

        //                    //show mess calib success
        //                    messageController.TogglePopupCalibSuccess(1, 0.5f);

        //                    //Set bool
        //                    isTrackedMark1 = false;
        //                    sttMark1 = false;
        //                    sttMark2 = true;
        //                    sttMark3 = true;
        //                    messageController.isCalibMess = false;
        //                }
        //            }
        //        }
        //        else if (time.Minute == 1 || time.Minute == 11 || time.Minute == 21 || time.Minute == 31 || time.Minute == 41 || time.Minute == 51)
        //        {
        //            if (!sttMark1)
        //            {
        //                if (spawnedObject == null)
        //                {
        //                    spawnedObject = Instantiate(objShow5mins, marker01.position, Quaternion.identity);
        //                }
        //                else
        //                {
        //                    Destroy(spawnedObject);
        //                    spawnedObject = Instantiate(objShow5mins, marker01.position, Quaternion.identity);
        //                }
        //                messageController.TogglePopupCalibSuccess(0, 0.5f);
        //                sttMark1 = true;
        //            }
        //        }
        //        else if (time.Minute == 56)
        //        {
        //            Destroy(spawnedObject);
        //            objImgTarget3.SetActive(true);
        //            sttMark1 = true;
        //        }
        //        else if (time.Minute == 59)
        //        {
        //            if (isTrackedMark1)
        //            {
        //                if (sttMark1)
        //                {

        //                    if (spawnedObject != null)
        //                    {
        //                        Destroy(spawnedObject);
        //                    }

        //                    //show mess calib success
        //                    messageController.TogglePopupCalibSuccess(1, 0.5f);

        //                    //Set bool
        //                    isTrackedMark1 = false;
        //                    sttMark1 = false;
        //                    sttMark2 = true;
        //                    sttMark3 = true;
        //                    messageController.isCalibMess = false;
        //                }
        //            }
        //        }
        //    }
        //    else if (time.Hour == 0)
        //    {
        //        if (time.Minute == 0)
        //        {
        //            if (!sttMark1)
        //            {
        //                if (spawnedObject == null)
        //                {
        //                    spawnedObject = Instantiate(objShow10mins, marker01.position, Quaternion.identity);
        //                }
        //                else
        //                {
        //                    Destroy(spawnedObject);
        //                    spawnedObject = Instantiate(objShow10mins, marker01.position, Quaternion.identity);
        //                }
        //                messageController.isCalibMess = true;
        //                sttMark1 = true;
        //            }
        //        }
        //        else if (time.Minute == 10 || time.Minute == 20 || time.Minute == 30 || time.Minute == 40 || time.Minute == 50)
        //        {
        //            if (time.Second < 30)
        //            {
        //                objImgTarget3.SetActive(true);
        //            }
        //            else
        //            {
        //                if (!sttMark1)
        //                {
        //                    objImgTarget3.SetActive(false);
        //                }
        //            }
        //            if (isTrackedMark1)
        //            {
        //                if (sttMark1)
        //                {
        //                    if (time.Second < 57)
        //                    {
        //                        if (spawnedObject == null)
        //                        {
        //                            spawnedObject = Instantiate(objPreshow, marker01.position, Quaternion.identity);
        //                        }
        //                        else
        //                        {
        //                            Destroy(spawnedObject);
        //                            spawnedObject = Instantiate(objPreshow, marker01.position, Quaternion.identity);
        //                        }
        //                    }

        //                    //show mess calib success
        //                    messageController.TogglePopupCalibSuccess(1, 0.5f);

        //                    //Set bool
        //                    isTrackedMark1 = false;
        //                    sttMark1 = false;
        //                    sttMark2 = true;
        //                    sttMark3 = true;
        //                    messageController.isCalibMess = false;
        //                }
        //            }
        //        }
        //        else if (time.Minute == 11 || time.Minute == 21 || time.Minute == 31 || time.Minute == 41 || time.Minute == 51)
        //        {
        //            if (!sttMark1)
        //            {
        //                if (spawnedObject == null)
        //                {
        //                    spawnedObject = Instantiate(objShow5mins, marker01.position, Quaternion.identity);
        //                }
        //                else
        //                {
        //                    Destroy(spawnedObject);
        //                    spawnedObject = Instantiate(objShow5mins, marker01.position, Quaternion.identity);
        //                }
        //                messageController.TogglePopupCalibSuccess(0, 0.5f);
        //                sttMark1 = true;
        //            }
        //        }
        //    }
        //}

        //bool sttMark2 = true;
        //void CheckMarker02(DateTime time)
        //{
        //    if (time.Hour == 1 || time.Hour == 2 || time.Hour == 3 || time.Hour == 4 || time.Hour == 5 || time.Hour == 6 || time.Hour == 7 || time.Hour == 8 || time.Hour == 9
        //        || time.Hour == 10 || time.Hour == 11 || time.Hour == 12 || time.Hour == 13 || time.Hour == 14 || time.Hour == 15 || time.Hour == 16 || time.Hour == 17 || time.Hour == 18
        //         || time.Hour == 19 || time.Hour == 20 || time.Hour == 21 || time.Hour == 22)
        //    {
        //        if (time.Minute == 6 || time.Minute == 16 || time.Minute == 26 || time.Minute == 36 || time.Minute == 46 || time.Minute == 56)
        //        {
        //            if (time.Second < 30)
        //            {
        //                objImgTarget3.SetActive(true);
        //            }
        //            else
        //            {
        //                if (!sttMark2)
        //                {
        //                    objImgTarget3.SetActive(false);
        //                }
        //            }
        //            if (isTrackedMark2)
        //            {
        //                if (sttMark2)
        //                {
        //                    if (time.Second < 57)
        //                    {
        //                        if (spawnedObject != null)
        //                        {
        //                            Destroy(spawnedObject);
        //                        }
        //                    }
        //                    //show mess calib success
        //                    messageController.TogglePopupCalibSuccess(1, 0.5f);

        //                    //Set bool
        //                    isTrackedMark2 = false;
        //                    sttMark2 = false;
        //                    sttMark1 = true;
        //                    sttMark3 = true;
        //                    messageController.isCalibMess = false;
        //                    messageController.isTrackWall2 = true;


        //                }
        //            }
        //        }
        //        else if (time.Minute == 7 || time.Minute == 17 || time.Minute == 27 || time.Minute == 37 || time.Minute == 47 || time.Minute == 57)
        //        {
        //            if (!sttMark2)
        //            {
        //                messageController.TogglePopupCalibSuccess(0, 0.5f);
        //                messageController.isCalibMess = true;
        //                messageController.isTrackWall2 = false;
        //                if (spawnedObject == null)
        //                {
        //                    spawnedObject = Instantiate(objCrystals, marker02.position, Quaternion.identity);
        //                }
        //                else
        //                {
        //                    Destroy(spawnedObject);
        //                    spawnedObject = Instantiate(objCrystals, marker02.position, Quaternion.identity);
        //                }
        //                sttMark2 = true;
        //            }
        //        }
        //    }
        //    else if (time.Hour == 23)
        //    {
        //        if (time.Minute == 6 || time.Minute == 16 || time.Minute == 26 || time.Minute == 36 || time.Minute == 46)
        //        {
        //            if (time.Second < 30)
        //            {
        //                objImgTarget3.SetActive(true);
        //            }
        //            else
        //            {
        //                if (!sttMark2)
        //                {
        //                    objImgTarget3.SetActive(false);
        //                }
        //            }
        //            if (isTrackedMark2)
        //            {
        //                if (sttMark2)
        //                {
        //                    if (time.Second < 57)
        //                    {
        //                        if (spawnedObject != null)
        //                        {
        //                            Destroy(spawnedObject);
        //                        }
        //                    }
        //                    //show mess calib success
        //                    messageController.TogglePopupCalibSuccess(1, 0.5f);

        //                    //Set bool
        //                    isTrackedMark2 = false;
        //                    sttMark2 = false;
        //                    sttMark1 = true;
        //                    sttMark3 = true;
        //                    messageController.isCalibMess = false;
        //                    messageController.isTrackWall2 = true;
        //                }
        //            }
        //        }
        //        else if (time.Minute == 7 || time.Minute == 17 || time.Minute == 27 || time.Minute == 37 || time.Minute == 47)
        //        {
        //            if (!sttMark2)
        //            {
        //                messageController.TogglePopupCalibSuccess(0, 0.5f);
        //                messageController.isCalibMess = true;
        //                messageController.isTrackWall2 = false;
        //                if (spawnedObject == null)
        //                {
        //                    spawnedObject = Instantiate(objCrystals, marker02.position, Quaternion.identity);
        //                }
        //                else
        //                {
        //                    Destroy(spawnedObject);
        //                    spawnedObject = Instantiate(objCrystals, marker02.position, Quaternion.identity);
        //                }
        //                sttMark2 = true;
        //            }
        //        }
        //    }
        //    else if (time.Hour == 0)
        //    {
        //        if (time.Minute == 16 || time.Minute == 26 || time.Minute == 36 || time.Minute == 46 || time.Minute == 56)
        //        {
        //            if (time.Second < 30)
        //            {
        //                objImgTarget3.SetActive(true);
        //            }
        //            else
        //            {
        //                if (!sttMark2)
        //                {
        //                    objImgTarget3.SetActive(false);
        //                }
        //            }
        //            if (isTrackedMark2)
        //            {
        //                if (sttMark2)
        //                {
        //                    if (time.Second < 57)
        //                    {
        //                        if (spawnedObject != null)
        //                        {
        //                            Destroy(spawnedObject);
        //                        }
        //                    }
        //                    //show mess calib success
        //                    messageController.TogglePopupCalibSuccess(1, 0.5f);

        //                    //Set bool
        //                    isTrackedMark2 = false;
        //                    sttMark2 = false;
        //                    sttMark1 = true;
        //                    sttMark3 = true;
        //                    messageController.isCalibMess = false;
        //                    messageController.isTrackWall2 = true;
        //                }
        //            }
        //        }
        //        else if (time.Minute == 17 || time.Minute == 27 || time.Minute == 37 || time.Minute == 47 || time.Minute == 57)
        //        {
        //            if (!sttMark2)
        //            {
        //                messageController.TogglePopupCalibSuccess(0, 0.5f);
        //                messageController.isCalibMess = true;
        //                messageController.isTrackWall2 = false;
        //                if (spawnedObject == null)
        //                {
        //                    spawnedObject = Instantiate(objCrystals, marker02.position, Quaternion.identity);
        //                }
        //                else
        //                {
        //                    Destroy(spawnedObject);
        //                    spawnedObject = Instantiate(objCrystals, marker02.position, Quaternion.identity);
        //                }
        //                sttMark2 = true;
        //            }
        //        }
        //    }

        //}

        //bool sttMark3 = true;
        //void CheckMarker03()
        //{
        //    if (isTrackedMark3)
        //    {
        //        if (sttMark3)
        //        {

        //            if (spawnedObject == null)
        //            {
        //                spawnedObject = Instantiate(objBanner, marker01.position, Quaternion.identity);
        //                StartCoroutine(LoopBanner());
        //            }
        //            else
        //            {
        //                Destroy(spawnedObject);
        //                spawnedObject = Instantiate(objBanner, marker01.position, Quaternion.identity);
        //                StartCoroutine(LoopBanner());
        //            }

        //            //show mess calib success
        //            messageController.TogglePopupCalibSuccess(1, 0.5f);

        //            //Set bool
        //            isTrackedMark3 = false;
        //            sttMark1 = true;
        //            sttMark2 = true;
        //            sttMark3 = false;
        //            messageController.isCalibMess = false;
        //        }
        //    }
        //}

        //int i = 0;
        //IEnumerator LoopBanner()
        //{
        //    yield return new WaitForSeconds(1);
        //    if (i < 80)
        //    {
        //        if (!sttMark3)
        //        {
        //            i += 1;
        //            StartCoroutine(LoopBanner());
        //        }
        //        else
        //        {
        //            i = 0;
        //            StopCoroutine(LoopBanner());
        //        }
        //    }
        //    else
        //    {
        //        Destroy(spawnedObject);
        //        spawnedObject = Instantiate(objBanner, marker01.position, Quaternion.identity);
        //        i = 0;
        //        StartCoroutine(LoopBanner());
        //    }
        //}

        //public void StopAllModel()
        //{
        //    Destroy(spawnedObject);
        //    sttMark1 = true;
        //    sttMark2 = true;
        //    sttMark3 = true;
        //}
    }
}