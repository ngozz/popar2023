using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMultipleMeshesParticle : MonoBehaviour
{
    private ParticleSystem ps;
    private ParticleSystemRenderer rd;

    public GameObject TextList;
    GameObject[] Child = new GameObject[100];

    // Start is called before the first frame update
    void Start()
    {
        // Get parent's Particle System Renderer
        ps = GetComponent<ParticleSystem>();
        rd = GetComponent<ParticleSystemRenderer>();

        // Prepare meshes
        Mesh[] meshes = new Mesh[100];

        for (int i = 0; i <= 93; i++)
        {
            Child[i] = TextList.transform.GetChild(i).gameObject;
            
            meshes[i] = ((MeshFilter)Child[i].GetComponent("MeshFilter")).sharedMesh;
        }


        //GameObject randomChild = Child[Random.Range(0, Child.Length - 1)];

        //MeshFilter mf = randomChild.getComponent("MeshFilter");

        rd.SetMeshes(meshes);
        Debug.Log(Child.Length);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

