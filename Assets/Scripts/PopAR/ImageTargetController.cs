// using System;
// using System.Collections;
// using UnityEngine;

// namespace JohnBui
// {
//     public class ImageTargetController : MonoBehaviour
//     {
//         [SerializeField] Transform marker01;
//         [SerializeField] Transform marker02;
//         [SerializeField] Transform marker03;


//         [SerializeField] GameObject objPreshow, objShow5mins, objShow10mins;
//         [SerializeField] GameObject objCrystals;
//         [SerializeField] GameObject objBanner;

//         [SerializeField] GameObject objImgTarget3;

//         // [SerializeField] MessageController messageController;

//         public bool isTrackedMark1 = false, isTrackedMark2 = false, isTrackedMark3 = false;

//         GameObject spawnedObject;

//         public bool isDetec = false;
//         bool isConnect = true;

//         private void Start()
//         {
//             StartCoroutine(checkInternetConnection());
//         }

//         private void Update()
//         {
//             DateTime currentDateTime = WorldTimeAPI.Instance.GetCurrentDateTime();
//             Debug.Log(currentDateTime.Hour + " : " + currentDateTime.Minute + " : " + currentDateTime.Second);
//             DateTime now = DateTime.Now;
//             if (isConnect)
//             {
//                 CheckMarker01(currentDateTime);
//                 CheckMarker02(currentDateTime);
//             }
//             else
//             {
//                 CheckMarker01(now);
//                 CheckMarker02(now);
//             }
//             CheckMarker03();
//             if (spawnedObject != null || !sttMark1 || !sttMark2 || !sttMark3)
//             {
//                 isDetec = true;
//             }
//             else if (spawnedObject == null && sttMark1 && sttMark2 && sttMark3)
//             {
//                 isDetec = false;
//             }

//         }

//         IEnumerator checkInternetConnection()
//         {
//             WWW www = new WWW("http://google.com");
//             yield return www;
//             if (www.error != null)
//             {
//                 isConnect = false;
//             }
//             else
//             {
//                 isConnect = true;
//             }
//         }

//         bool sttMark1 = true;
//         void CheckMarker01(DateTime time)
//         {
//             if (time.Hour == 1 || time.Hour == 2 || time.Hour == 3 || time.Hour == 4 || time.Hour == 5 || time.Hour == 6 || time.Hour == 7 || time.Hour == 8 || time.Hour == 9
//                 || time.Hour == 10 || time.Hour == 11 || time.Hour == 12 || time.Hour == 13 || time.Hour == 14 || time.Hour == 15 || time.Hour == 16 || time.Hour == 17 || time.Hour == 18
//                  || time.Hour == 19 || time.Hour == 20 || time.Hour == 21 || time.Hour == 22)
//             {
//                 if (time.Minute == 0 || time.Minute == 10 || time.Minute == 20 || time.Minute == 30 || time.Minute == 40 || time.Minute == 50)
//                 {
//                     if (time.Second < 30)
//                     {
//                         objImgTarget3.SetActive(true);
//                     }
//                     else
//                     {
//                         if (!sttMark1)
//                         {
//                             objImgTarget3.SetActive(false);
//                         }
//                     }
//                     //marker 1 tracked found
//                     if (isTrackedMark1)
//                     {
//                         if (sttMark1)
//                         {
//                             if (time.Second < 57)
//                             {
//                                 if (spawnedObject == null)
//                                 {
//                                     spawnedObject = Instantiate(objPreshow, marker01.position, Quaternion.identity);
//                                 }
//                                 else
//                                 {
//                                     Destroy(spawnedObject);
//                                     spawnedObject = Instantiate(objPreshow, marker01.position, Quaternion.identity);
//                                 }
//                             }

//                             //show mess calib success
//                             MessageManager.Instance.ActivePopupSuccess();

//                             //Set bool
//                             isTrackedMark1 = false;
//                             sttMark1 = false;
//                             sttMark2 = true;
//                             sttMark3 = true;
//                         }
//                     }
//                 }
//                 else if (time.Minute == 1 || time.Minute == 11 || time.Minute == 21 || time.Minute == 31 || time.Minute == 41 || time.Minute == 51)
//                 {
//                     if (!isTrackedMark1)
//                     {
//                         objImgTarget3.SetActive(false);
//                         if (spawnedObject == null)
//                         {
//                             spawnedObject = Instantiate(objShow5mins, marker01.position, Quaternion.identity);
//                         }
//                         else
//                         {
//                             Destroy(spawnedObject);
//                             spawnedObject = Instantiate(objShow5mins, marker01.position, Quaternion.identity);
//                         }
//                         MessageManager.Instance.ActivePopupSuccess();
//                         sttMark1 = true;
//                     }
//                 }
//             }
//             else if (time.Hour == 23)
//             {
//                 if (time.Minute == 0 || time.Minute == 10 || time.Minute == 20 || time.Minute == 30 || time.Minute == 40 || time.Minute == 50)
//                 {
//                     if (time.Second < 30)
//                     {
//                         objImgTarget3.SetActive(true);
//                     }
//                     else
//                     {
//                         if (!sttMark1)
//                         {
//                             objImgTarget3.SetActive(false);
//                         }
//                     }
//                     if (isTrackedMark1)
//                     {
//                         if (sttMark1)
//                         {
//                             if (time.Second < 57)
//                             {
//                                 if (spawnedObject == null)
//                                 {
//                                     spawnedObject = Instantiate(objPreshow, marker01.position, Quaternion.identity);
//                                 }
//                                 else
//                                 {
//                                     Destroy(spawnedObject);
//                                     spawnedObject = Instantiate(objPreshow, marker01.position, Quaternion.identity);
//                                 }
//                             }

//                             //show mess calib success
//                             MessageManager.Instance.ActivePopupSuccess();

//                             //Set bool
//                             isTrackedMark1 = false;
//                             sttMark1 = false;
//                             sttMark2 = true;
//                             sttMark3 = true;
//                         }
//                     }
//                 }
//                 else if (time.Minute == 1 || time.Minute == 11 || time.Minute == 21 || time.Minute == 31 || time.Minute == 41 || time.Minute == 51)
//                 {
//                     if (!sttMark1)
//                     {
//                         if (spawnedObject == null)
//                         {
//                             spawnedObject = Instantiate(objShow5mins, marker01.position, Quaternion.identity);
//                         }
//                         else
//                         {
//                             Destroy(spawnedObject);
//                             spawnedObject = Instantiate(objShow5mins, marker01.position, Quaternion.identity);
//                         }
//                         MessageManager.Instance.ActivePopupSuccess();
//                         sttMark1 = true;
//                     }
//                 }
//                 else if (time.Minute == 56)
//                 {
//                     Destroy(spawnedObject);
//                     objImgTarget3.SetActive(true);
//                     sttMark1 = true;
//                 }
//                 else if (time.Minute == 59)
//                 {
//                     if (isTrackedMark1)
//                     {
//                         if (sttMark1)
//                         {

//                             if (spawnedObject != null)
//                             {
//                                 Destroy(spawnedObject);
//                             }

//                             //show mess calib success
//                             MessageManager.Instance.ActivePopupSuccess();

//                             //Set bool
//                             isTrackedMark1 = false;
//                             sttMark1 = false;
//                             sttMark2 = true;
//                             sttMark3 = true;
//                         }
//                     }
//                 }
//             }
//             else if (time.Hour == 0)
//             {
//                 if (time.Minute == 0)
//                 {
//                     if (!sttMark1)
//                     {
//                         if (spawnedObject == null)
//                         {
//                             spawnedObject = Instantiate(objShow10mins, marker01.position, Quaternion.identity);
//                         }
//                         else
//                         {
//                             Destroy(spawnedObject);
//                             spawnedObject = Instantiate(objShow10mins, marker01.position, Quaternion.identity);
//                         }
//                         sttMark1 = true;
//                     }
//                 }
//                 else if (time.Minute == 10 || time.Minute == 20 || time.Minute == 30 || time.Minute == 40 || time.Minute == 50)
//                 {
//                     if (time.Second < 30)
//                     {
//                         objImgTarget3.SetActive(true);
//                     }
//                     else
//                     {
//                         if (!sttMark1)
//                         {
//                             objImgTarget3.SetActive(false);
//                         }
//                     }
//                     if (isTrackedMark1)
//                     {
//                         if (sttMark1)
//                         {
//                             if (time.Second < 57)
//                             {
//                                 if (spawnedObject == null)
//                                 {
//                                     spawnedObject = Instantiate(objPreshow, marker01.position, Quaternion.identity);
//                                 }
//                                 else
//                                 {
//                                     Destroy(spawnedObject);
//                                     spawnedObject = Instantiate(objPreshow, marker01.position, Quaternion.identity);
//                                 }
//                             }

//                             //show mess calib success
//                             MessageManager.Instance.ActivePopupSuccess();

//                             //Set bool
//                             isTrackedMark1 = false;
//                             sttMark1 = false;
//                             sttMark2 = true;
//                             sttMark3 = true;
//                         }
//                     }
//                 }
//                 else if (time.Minute == 11 || time.Minute == 21 || time.Minute == 31 || time.Minute == 41 || time.Minute == 51)
//                 {
//                     if (!sttMark1)
//                     {
//                         if (spawnedObject == null)
//                         {
//                             spawnedObject = Instantiate(objShow5mins, marker01.position, Quaternion.identity);
//                         }
//                         else
//                         {
//                             Destroy(spawnedObject);
//                             spawnedObject = Instantiate(objShow5mins, marker01.position, Quaternion.identity);
//                         }
//                         MessageManager.Instance.ActivePopupSuccess();
//                         sttMark1 = true;
//                     }
//                 }
//             }
//         }

//         bool sttMark2 = true;
//         void CheckMarker02(DateTime time)
//         {
//             if (time.Hour == 1 || time.Hour == 2 || time.Hour == 3 || time.Hour == 4 || time.Hour == 5 || time.Hour == 6 || time.Hour == 7 || time.Hour == 8 || time.Hour == 9
//                 || time.Hour == 10 || time.Hour == 11 || time.Hour == 12 || time.Hour == 13 || time.Hour == 14 || time.Hour == 15 || time.Hour == 16 || time.Hour == 17 || time.Hour == 18
//                  || time.Hour == 19 || time.Hour == 20 || time.Hour == 21 || time.Hour == 22)
//             {
//                 if (time.Minute == 6 || time.Minute == 16 || time.Minute == 26 || time.Minute == 36 || time.Minute == 46 || time.Minute == 56)
//                 {
//                     if (time.Second < 30)
//                     {
//                         objImgTarget3.SetActive(true);
//                     }
//                     else
//                     {
//                         if (!sttMark2)
//                         {
//                             objImgTarget3.SetActive(false);
//                         }
//                     }
//                     if (isTrackedMark2)
//                     {
//                         if (sttMark2)
//                         {
//                             if (time.Second < 57)
//                             {
//                                 if (spawnedObject != null)
//                                 {
//                                     Destroy(spawnedObject);
//                                 }
//                             }
//                             //show mess calib success
//                             MessageManager.Instance.ActivePopupSuccess();

//                             //Set bool
//                             isTrackedMark2 = false;
//                             sttMark2 = false;
//                             sttMark1 = true;
//                             sttMark3 = true;


//                         }
//                     }
//                 }
//                 else if (time.Minute == 7 || time.Minute == 17 || time.Minute == 27 || time.Minute == 37 || time.Minute == 47 || time.Minute == 57)
//                 {
//                     if (!sttMark2)
//                     {
//                         MessageManager.Instance.ActivePopupSuccess();
//                         if (spawnedObject == null)
//                         {
//                             spawnedObject = Instantiate(objCrystals, marker02.position, Quaternion.identity);
//                         }
//                         else
//                         {
//                             Destroy(spawnedObject);
//                             spawnedObject = Instantiate(objCrystals, marker02.position, Quaternion.identity);
//                         }
//                         sttMark2 = true;
//                     }
//                 }
//             }
//             else if (time.Hour == 23)
//             {
//                 if (time.Minute == 6 || time.Minute == 16 || time.Minute == 26 || time.Minute == 36 || time.Minute == 46)
//                 {
//                     if (time.Second < 30)
//                     {
//                         objImgTarget3.SetActive(true);
//                     }
//                     else
//                     {
//                         if (!sttMark2)
//                         {
//                             objImgTarget3.SetActive(false);
//                         }
//                     }
//                     if (isTrackedMark2)
//                     {
//                         if (sttMark2)
//                         {
//                             if (time.Second < 57)
//                             {
//                                 if (spawnedObject != null)
//                                 {
//                                     Destroy(spawnedObject);
//                                 }
//                             }
//                             //show mess calib success
//                             MessageManager.Instance.ActivePopupSuccess();

//                             //Set bool
//                             isTrackedMark2 = false;
//                             sttMark2 = false;
//                             sttMark1 = true;
//                             sttMark3 = true;
//                         }
//                     }
//                 }
//                 else if (time.Minute == 7 || time.Minute == 17 || time.Minute == 27 || time.Minute == 37 || time.Minute == 47)
//                 {
//                     if (!sttMark2)
//                     {
//                         MessageManager.Instance.ActivePopupSuccess();
//                         if (spawnedObject == null)
//                         {
//                             spawnedObject = Instantiate(objCrystals, marker02.position, Quaternion.identity);
//                         }
//                         else
//                         {
//                             Destroy(spawnedObject);
//                             spawnedObject = Instantiate(objCrystals, marker02.position, Quaternion.identity);
//                         }
//                         sttMark2 = true;
//                     }
//                 }
//             }
//             else if (time.Hour == 0)
//             {
//                 if (time.Minute == 16 || time.Minute == 26 || time.Minute == 36 || time.Minute == 46 || time.Minute == 56)
//                 {
//                     if (time.Second < 30)
//                     {
//                         objImgTarget3.SetActive(true);
//                     }
//                     else
//                     {
//                         if (!sttMark2)
//                         {
//                             objImgTarget3.SetActive(false);
//                         }
//                     }
//                     if (isTrackedMark2)
//                     {
//                         if (sttMark2)
//                         {
//                             if (time.Second < 57)
//                             {
//                                 if (spawnedObject != null)
//                                 {
//                                     Destroy(spawnedObject);
//                                 }
//                             }
//                             //show mess calib success
//                             MessageManager.Instance.ActivePopupSuccess();

//                             //Set bool
//                             isTrackedMark2 = false;
//                             sttMark2 = false;
//                             sttMark1 = true;
//                             sttMark3 = true;
//                         }
//                     }
//                 }
//                 else if (time.Minute == 17 || time.Minute == 27 || time.Minute == 37 || time.Minute == 47 || time.Minute == 57)
//                 {
//                     if (!sttMark2)
//                     {
//                         MessageManager.Instance.ActivePopupSuccess();
//                         if (spawnedObject == null)
//                         {
//                             spawnedObject = Instantiate(objCrystals, marker02.position, Quaternion.identity);
//                         }
//                         else
//                         {
//                             Destroy(spawnedObject);
//                             spawnedObject = Instantiate(objCrystals, marker02.position, Quaternion.identity);
//                         }
//                         sttMark2 = true;
//                     }
//                 }
//             }

//         }

//         bool sttMark3 = true;
//         void CheckMarker03()
//         {
//             if (isTrackedMark3)
//             {
//                 if (sttMark3)
//                 {

//                     if (spawnedObject == null)
//                     {
//                         spawnedObject = Instantiate(objBanner, marker01.position, Quaternion.identity);
//                         StartCoroutine(LoopBanner());
//                     }
//                     else
//                     {
//                         Destroy(spawnedObject);
//                         spawnedObject = Instantiate(objBanner, marker01.position, Quaternion.identity);
//                         StartCoroutine(LoopBanner());
//                     }

//                     //show mess calib success
//                     MessageManager.Instance.ActivePopupSuccess();

//                     //Set bool
//                     isTrackedMark3 = false;
//                     sttMark1 = true;
//                     sttMark2 = true;
//                     sttMark3 = false;
//                 }
//             }
//         }

//         int i = 0;
//         IEnumerator LoopBanner()
//         {
//             yield return new WaitForSeconds(1);
//             if (i < 80)
//             {
//                 if (!sttMark3)
//                 {
//                     i += 1;
//                     StartCoroutine(LoopBanner());
//                 }
//                 else
//                 {
//                     i = 0;
//                     StopCoroutine(LoopBanner());
//                 }
//             }
//             else
//             {
//                 Destroy(spawnedObject);
//                 spawnedObject = Instantiate(objBanner, marker01.position, Quaternion.identity);
//                 i = 0;
//                 StartCoroutine(LoopBanner());
//             }
//         }

//         public void StopAllModel()
//         {
//             Destroy(spawnedObject);
//             sttMark1 = true;
//             sttMark2 = true;
//             sttMark3 = true;
//         }
//     }
// }