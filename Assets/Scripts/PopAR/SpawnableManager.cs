using UnityEngine;

public class SpawnableManager : MonoBehaviour
{
    [SerializeField]
    GameObject spawnablePrefab;

    public GameObject spawnedObject;

    void Start()
    {
        spawnedObject = null;
    }

    public void SpawnPrefab(Vector3 spawnPosition)
    {
        if(spawnedObject == null)
        {
            spawnedObject = Instantiate(spawnablePrefab, spawnPosition, Quaternion.identity);
        }      
    }
}
