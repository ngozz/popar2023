using DG.Tweening;
using TMPro;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Playables;

namespace JohnBui
{
    public class MessageManager : MonoBehaviour
    {
        [SerializeField] public CanvasGroup popupPointTo, popupSuccess, popupFail, popupSafe, popupPhoto;

        public static MessageManager Instance;

        private float countdownTime = 0f, timeDetectFail = 50f;
        private float secondStartScene;

        DateTime now = DateTime.Now;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
            }
            else
            {
                Instance = this;
            }
        }
        public void resetStatus()
        {
            countdownTime = 0f;
            popupPointTo.gameObject.SetActive(false);
            popupSuccess.gameObject.SetActive(false);
            popupFail.gameObject.SetActive(false);
            popupSafe.gameObject.SetActive(false);
            popupPhoto.gameObject.SetActive(false);
            ActivePopupPointTo();
        }
        private void Start()
        {
            secondStartScene = now.Second;
            countdownTime = 0f;
            popupPointTo.gameObject.SetActive(false);
            popupSuccess.gameObject.SetActive(false);
            popupFail.gameObject.SetActive(false);
            popupSafe.gameObject.SetActive(false);
            popupPhoto.gameObject.SetActive(false);
            ActivePopupSaveAndPhoto();
        }

        private void Update()
        {
            now = DateTime.Now;
            if(popupSuccess.gameObject.activeInHierarchy)
            {
                Debug.Log("pop up success: "+now);
            }
            // 50s hien 1 lan fail 10s 
            if (popupPointTo.gameObject.activeInHierarchy)
            {
                countdownTime += Time.unscaledDeltaTime;
                if(countdownTime >= timeDetectFail)
                {
                    countdownTime = 0;
                    ActivePopupFail();
                }
            }
        }

        public void ActivePopupSaveAndPhoto()
        {
            popupSafe.gameObject.SetActive(true);
            popupSafe.DOFade(1f, 0.5f).SetDelay(1f).OnComplete(() =>
            {
                popupSafe.DOFade(0f, 0.5f).SetDelay(6f).OnComplete(() => 
                { 
                    popupSafe.gameObject.SetActive(false); 
                });
            });
            popupPhoto.gameObject.SetActive(true);
            popupPhoto.DOFade(1f, 0.5f).SetDelay(1f).OnComplete(() =>
            {
                popupPhoto.DOFade(0f, 0.5f).SetDelay(6f).OnComplete(() =>
                {
                    popupPhoto.gameObject.SetActive(false);
                    ActivePopupPointTo();
                });
            });
        }

        public void ActivePopupPointTo()
        {
            if(!popupPhoto.gameObject.activeInHierarchy 
            && !popupFail.gameObject.activeInHierarchy
            && !popupSuccess.gameObject.activeInHierarchy)
            {
                popupPointTo.DOFade(1f, 0.5f).OnComplete(() =>
                {
                    popupPointTo.gameObject.SetActive(true);
                });
            }
        }
        
        public void ActivePopupSuccess()
        {
            if(popupPhoto.gameObject.activeInHierarchy)
            {   
                float delayTime = now.Second;
                if (delayTime > secondStartScene){
                    delayTime = 20f - (delayTime - secondStartScene);
                } else {
                    delayTime = 20f - (delayTime + 60 - secondStartScene);
                }
                popupSuccess.DOFade(0f, 0f).SetDelay(delayTime).OnComplete(() =>
                {
                    Debug.Log("delay time" + delayTime);
                    if(popupPointTo.gameObject.activeInHierarchy)
                    {
                        DeactivePopupPointTo();
                        popupSuccess.gameObject.SetActive(true);
                        popupSuccess.DOFade(1f, 0.5f).SetDelay(1f).OnComplete(() =>
                        {
                            popupSuccess.DOFade(0f, 0.5f).SetDelay(8f).OnComplete(() => {
                                DeactivePopupSuccess();
                            });
                        });
                    }
                });
            }
            else 
            {
                if(popupSuccess.gameObject.activeInHierarchy)
                {
                    popupSuccess.DOFade(0f, 0.5f).OnComplete(() => 
                    {
                        popupSuccess.DOFade(1f, 0.5f).SetDelay(0.5f).OnComplete(() =>
                        {
                            popupSuccess.DOFade(0f, 0.5f).SetDelay(8f).OnComplete(() => 
                            {
                                DeactivePopupSuccess();
                            });
                        });
                    });
                } 
                else
                {
                    popupSuccess.DOFade(0f, 0f).SetDelay(1f).OnComplete(() =>
                    {
                        if(popupPointTo.gameObject.activeInHierarchy)
                        {
                            DeactivePopupPointTo();
                        } else if(popupFail.gameObject.activeInHierarchy)
                        {
                            DeactivePopupFail();
                        }
                        popupSuccess.gameObject.SetActive(true);
                        popupSuccess.DOFade(1f, 0.5f).SetDelay(1f).OnComplete(() =>
                        {
                            popupSuccess.DOFade(0f, 0.5f).SetDelay(8f).OnComplete(() => 
                            {
                                DeactivePopupSuccess();
                            });
                        });
                    });
                }
            }
        }
        public void ActivePopupFail()
        {
            if(popupPointTo.gameObject.activeInHierarchy)
            {
                DeactivePopupPointTo();
                popupFail.gameObject.SetActive(true);
                popupFail.DOFade(1f, 0.5f).SetDelay(1f).OnComplete(() =>
                {
                    popupFail.DOFade(0f, 0.5f).SetDelay(8f).OnComplete(() => {
                        DeactivePopupFail();
                    });
                });
            }
        }

        public void DeactivePopupPointTo()
        {
            popupPointTo.DOFade(0f, 0.5f).OnComplete(() => { popupPointTo.gameObject.SetActive(false); });
        }
        public void DeactivePopupSuccess()
        {
            popupSuccess.DOFade(0f, 0.5f).OnComplete(() => {popupSuccess.gameObject.SetActive(false);});
        }
        public void DeactivePopupFail()
        {
            popupFail.DOFade(0f, 0.5f).OnComplete(() => 
            {
                popupFail.gameObject.SetActive(false);
                ActivePopupPointTo();
            });
        }
    }
}