using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace JohnBui
{
    public class ReloadScene : MonoBehaviour
    {
        public GameObject Snow;
        public GameObject Button;

        private bool IsActive = false;
        // public Sprite show, hidden;
        public void testButton()
        {
            if(Snow.activeInHierarchy)
            {
                Debug.Log("Position: inactive");
                IsActive = false;
                // Button.GetComponent<Image>().sprite = hidden;
                Snow.SetActive(false);
                StartesManagerVer2.Instance.resetStatus();
                ActiveContent.Instance.resetStatus();
                MessageManager.Instance.resetStatus();
            } else {
                
                IsActive = true;
                Snow.SetActive(true);
                ActiveContent.Instance.PreShow.SetActive(false);
                ActiveContent.Instance.MainShow.SetActive(false);
                ActiveContent.Instance.Crystal.SetActive(false);
                ActiveContent.Instance.KV.SetActive(false);
                ActiveContent.Instance.Show10minFireWork.SetActive(false);
                MessageManager.Instance.popupPointTo.gameObject.SetActive(false);
                MessageManager.Instance.ActivePopupSuccess();
                MessageManager.Instance.popupFail.gameObject.SetActive(false);
                MessageManager.Instance.popupSafe.gameObject.SetActive(false);
                MessageManager.Instance.popupPhoto.gameObject.SetActive(false);
                
                if(StartesManagerVer2.Instance.preTransform.position != null)
                {
                    Debug.Log("Position: active"+StartesManagerVer2.Instance.preTransform.position);
                    Snow.transform.position = StartesManagerVer2.Instance.preTransform.position;
                    Snow.transform.rotation = StartesManagerVer2.Instance.preTransform.rotation;
                } else 
                {
                    Snow.transform.position = new Vector3(0,0,50);
                    Snow.transform.rotation = new Quaternion(0,0,0,0);
                }

            }
        }

        public void reloadScene()
        {
            StartesManagerVer2.Instance.resetStatus();
            ActiveContent.Instance.resetStatus();
            MessageManager.Instance.resetStatus();
        }
    }
}
