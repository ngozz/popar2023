using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace JohnBui
{
    public class TestButton : MonoBehaviour
    {
        [SerializeField] public GameObject Snow;
        private bool IsActive = false;
        public void testButton()
        {
            if(IsActive)
            {
                IsActive = false;
                Snow.SetActive(false);
            } else {
                IsActive = true;
                Snow.SetActive(true);
            }
        }
    }
}
