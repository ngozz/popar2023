using UnityEngine;
using System;
using System.Collections;
using System.Text.RegularExpressions;
using UnityEngine.Networking;

namespace JohnBui
{
	public class TimeISAPI : MonoBehaviour
	{
		// #region Singleton class: TimeISAPI

		public static TimeISAPI Instance;
		public DateTime curTime;
		const string API_URL = "https://worldtimeapi.org/api/timezone/Asia/Singapore";

		private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
            }
            else
            {
                Instance = this;
            }
        }
		
		private void Update()
		{
			StartCoroutine(GetRequest(API_URL));
			Debug.Log(curTime);

		}

		IEnumerator GetRequest(string uri)
		{
			string timeText = "";
			using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
			{
				// Request and wait for the desired page.
				yield return webRequest.SendWebRequest();

				switch (webRequest.result)
				{
					case UnityWebRequest.Result.Success:
					 	// Debug.Log(webRequest.downloadHandler.text);
						timeText = webRequest.downloadHandler.text;
						string[] splitText = timeText.Split(',');
						if (splitText.Length >= 3)
						{
							timeText = splitText[2];
							splitText = timeText.Split('"');
							if (splitText.Length >= 2)
							{
								curTime = DateTime.Parse(splitText[3]);
								//getTimeCompleteTime = Time.unscaledTime;
							}
						}
						break;
					default:
						curTime = DateTime.Now;
						//getTimeCompleteTime = Time.unscaledTime;
						break;
				}

			}
		}
	}
}

