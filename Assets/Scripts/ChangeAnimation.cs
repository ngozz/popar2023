using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jacovone
{
    public class ChangeAnimation : MonoBehaviour
    {
        //[Serializable] ParticleSystem Fireworks;
        //EditorGUILayout.EndHorizontal();
        public PathMagic pathMagic;
        public Animator Animator;
        public float timeWaving;
        public Vector3[] pointsChangeAnimation;
        int i = 0;
        float time = 0;
        float velocity = 0;

        // Start is called before the first frame update
        void Start()
        {
            velocity = pathMagic.VelocityBias;
            Animator.SetFloat("Anim", 0f);
            Animator.speed = 1.5f;
        }

        // Update is called once per frame
        void Update()
        {      
            if(pathMagic != null)
            {   
                if(pathMagic.LastPassedWayponint + 1 == pointsChangeAnimation[i].x && pointsChangeAnimation[i] != null)// ||pathMagic.LastPassedWayponint + 2 == pointsChangeAnimation[i])
                {
                    
                    if (pointsChangeAnimation[i].y != 0f && time < timeWaving)
                    {   
                        Animator.SetFloat("Anim", pointsChangeAnimation[i].y);
                        Animator.speed = pointsChangeAnimation[i].z;
                        pathMagic.VelocityBias = velocity/30;
                        time += Time.deltaTime;
                        //Debug.Log(time.ToString("0.000"));
                    }

                    if (time >= timeWaving)
                    {
                        Animator.SetFloat("Anim", 0f);
                        Animator.speed = 1.5f;
                        pathMagic.VelocityBias = velocity;
                    }
                } else 
                {       
                    time = 0;
                    i++;
                    i %= pointsChangeAnimation.Length;
                    Animator.SetFloat("Anim", 0f);
                }
            }
        }
    }    
}

